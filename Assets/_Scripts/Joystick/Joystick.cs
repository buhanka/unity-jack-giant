﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Joystick : MonoBehaviour, IPointerUpHandler, IPointerDownHandler {

    [SerializeField]
    private PlayerMoveJoystick playerJoystick;

    public void OnPointerDown(PointerEventData data)
    {
        if (gameObject.name == "Left")
        {
            playerJoystick.SetSidesBasedOnLeft(true);
        }
        else
        {
            playerJoystick.SetSidesBasedOnLeft(false);
        }
    }

    public void OnPointerUp(PointerEventData data)
    {
        playerJoystick.StopMoving();
    }

}
