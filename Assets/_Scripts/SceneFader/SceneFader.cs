﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneFader : MonoBehaviour {

    public static SceneFader Instance;

    [SerializeField]
    private GameObject faderPanel;
    [SerializeField]
    private Animator faderAnim;

	private void Awake()
	{
        MakeSingleton();
	}

    private void MakeSingleton()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void ChangeSceneWithFader(string sceneName)
    {
        StartCoroutine(FadeInOutBetweenScenes(sceneName));
    }

    private IEnumerator FadeInOutBetweenScenes(string sceneName)
    {
        faderPanel.SetActive(true);
        faderAnim.Play("FadeIn");
        yield return StartCoroutine(RealTimeCoroutine.WaitForRealTimeSinceStart(0.7f));

        SceneManager.LoadScene(sceneName);

        faderAnim.Play("FadeOut");
        yield return StartCoroutine(RealTimeCoroutine.WaitForRealTimeSinceStart(0.5f));
        faderPanel.SetActive(false);
    }
}
