﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadyGameController : MonoBehaviour {

    //Start camera motion after Ready animation is end
    public void EnableTimeScale()
    {
        GameController.Instance.MoveCamera(true);
    }
}
