﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    //Public variables
    public static GameManager Instance;
    //

    //Private variables
    [HideInInspector]
    public bool gameStartedFromMainMenu, gameStartedAfterPlayerDied;
    [HideInInspector]
    public int score, coinScore, lifeScore;
    //

    /*Unity methods*/
	private void Awake() 
    {
        MakeSingleton();
	}

	private void Start()
	{
        InitializePreferences();
	}

	private void OnLevelWasLoaded()
	{
        if (SceneManager.GetActiveScene().name == "Gameplay")
        {
            if (gameStartedAfterPlayerDied)
            {
                GameController.Instance.SetScore(score);
                GameController.Instance.SetCoinScore(coinScore);
                GameController.Instance.SetLifeScore(lifeScore);

                PlayerScore.scoreCount = score;
                PlayerScore.coinCount = coinScore;
                PlayerScore.lifeCount = lifeScore;
            }
            else if (gameStartedFromMainMenu)
            {
                PlayerScore.scoreCount = 0;
                PlayerScore.coinCount = 0;
                PlayerScore.lifeCount = 2;

                GameController.Instance.SetScore(0);
                GameController.Instance.SetCoinScore(0);
                GameController.Instance.SetLifeScore(2);
            } 
        }
	}
	/**/

	private void MakeSingleton()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void InitializePreferences()
    {
        if (!PlayerPrefs.HasKey("Game Initialized"))
        {
            GamePreferences.SetEasyDifficultyState(0);
            GamePreferences.SetEasyDifficultyCoinScore(0);
            GamePreferences.SetEasyDifficultyHighScore(0);

            GamePreferences.SetMediumDifficultyState(1);
            GamePreferences.SetMediumDifficultyCoinScore(0);
            GamePreferences.SetMediumDifficultyHighScore(0);

            GamePreferences.SetHardDifficultyState(0);
            GamePreferences.SetHardDifficultyCoinScore(0);
            GamePreferences.SetHardDifficultyHighScore(0);

            GamePreferences.SetMusicState(0);

            PlayerPrefs.SetInt("Game Initialized", 1);
        }
    }

    public void CheckGameStatus(int score, int coinScore, int lifeScore)
    {
        if (lifeScore < 0)
        {
            SaveScoreAndCoinByDifficulty(score, coinScore);

            gameStartedFromMainMenu = false;
            gameStartedAfterPlayerDied = false;

            GameController.Instance.GameOverShowPanel(score, coinScore);
        }
        else
        {
            this.score = score;
            this.coinScore = coinScore;
            this.lifeScore = lifeScore;

            gameStartedFromMainMenu = false;
            gameStartedAfterPlayerDied = true;

            GameController.Instance.RestartGame();
        }
    }

    private void SaveScoreAndCoinByDifficulty(int currentScore, int currentCoinScore)
    {
        if (GamePreferences.GetEasyDifficultyState() == 1)
        {
            int highScoreTemp = GamePreferences.GetEasyDifficultyHighScore();
            int coinScoreTemp = GamePreferences.GetEasyDifficultyCoinScore();

            if (currentScore > highScoreTemp)
            {
                GamePreferences.SetEasyDifficultyHighScore(currentScore);
            }

            if (currentCoinScore > coinScoreTemp)
            {
                GamePreferences.SetEasyDifficultyCoinScore(currentCoinScore);
            }
        }

        if (GamePreferences.GetMediumDifficultyState() == 1)
        {
            int highScoreTemp = GamePreferences.GetMediumDifficultyHighScore();
            int coinScoreTemp = GamePreferences.GetMediumDifficultyCoinScore();

            if (currentScore > highScoreTemp)
            {
                GamePreferences.SetMediumDifficultyHighScore(currentScore);
            }

            if (currentCoinScore > coinScoreTemp)
            {
                GamePreferences.SetMediumDifficultyCoinScore(currentCoinScore);
            }
        }

        if (GamePreferences.GetHardDifficultyState() == 1)
        {
            int highScoreTemp = GamePreferences.GetHardDifficultyHighScore();
            int coinScoreTemp = GamePreferences.GetHardDifficultyCoinScore();

            if (currentScore > highScoreTemp)
            {
                GamePreferences.SetHardDifficultyHighScore(currentScore);
            }

            if (currentCoinScore > coinScoreTemp)
            {
                GamePreferences.SetHardDifficultyCoinScore(currentCoinScore);
            }
        }
    }
}
