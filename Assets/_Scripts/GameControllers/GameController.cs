﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    //Public variables
    public static GameController Instance;
    public bool cameraMotionState;
    //

    //Private variables
    [SerializeField]
    private Text scoreText, coinText, lifeText, totalScoreText, totalCoinText;
    [SerializeField]
    private GameObject pausePanel, gameOverPanel;
    public CameraMotion cameraMotionScript;
    //

    /*Unity methods*/
	private void Awake() 
    {
        MakeInstance();
	}

	private void Start()
	{
        cameraMotionState = cameraMotionScript.moveCamera;
	}
	/**/

	public void MakeInstance()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void SetScore(int score)
    {
        scoreText.text = score.ToString();
    }

    public void SetCoinScore(int coinScore)
    {
        coinText.text = "x" + coinScore;
    }

    public void SetLifeScore(int lifeScore)
    {
        lifeText.text = "x" + lifeScore;
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
        pausePanel.SetActive(true);
    }

    public void ResumeGame()
    {
        Time.timeScale = 1f;
        pausePanel.SetActive(false);
    }

    public void MoveCamera(bool isMove)
    {
        cameraMotionScript.moveCamera = isMove;
        cameraMotionState = isMove;
    }

    public void GameOverShowPanel(int totalScore, int totalCoin)
    {
        totalScoreText.text = totalScore.ToString();
        totalCoinText.text = totalCoin.ToString();

        Time.timeScale = 0f;
        gameOverPanel.SetActive(true);
    }

    public void RestartGame()
    {
        Time.timeScale = 1f;
        SceneFader.Instance.ChangeSceneWithFader("Gameplay");
    }

    public void QuitGame()
    {
        Time.timeScale = 1f;
        SceneFader.Instance.ChangeSceneWithFader("MainMenu");
    }
}
