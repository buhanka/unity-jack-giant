﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OptionsController : MonoBehaviour {

    [SerializeField]
    private GameObject easySign, mediumSign, hardSign;

	private void Start()
	{
        SetTheDifficulty();
	}

    private void SetInitialDifficultyMode(string difficulty)
    {
        switch (difficulty)
        {
            case "easy":
                easySign.SetActive(true);
                mediumSign.SetActive(false);
                hardSign.SetActive(false);
                break;
            case "medium":
                easySign.SetActive(false);
                mediumSign.SetActive(true);
                hardSign.SetActive(false);
                break;
            case "hard":
                easySign.SetActive(false);
                mediumSign.SetActive(false);
                hardSign.SetActive(true);
                break;
        }
    }

    private void SetTheDifficulty()
    {
        if (GamePreferences.GetEasyDifficultyState() == 1)
        {
            SetInitialDifficultyMode("easy");
        }

        if (GamePreferences.GetMediumDifficultyState() == 1)
        {
            SetInitialDifficultyMode("medium");
        }

        if (GamePreferences.GetHardDifficultyState() == 1)
        {
            SetInitialDifficultyMode("hard");
        }
    }

    private void SetDifficultyState(int easyState, int mediumState, int hardState)
    {
        GamePreferences.SetEasyDifficultyState(easyState);
        GamePreferences.SetMediumDifficultyState(mediumState);
        GamePreferences.SetHardDifficultyState(hardState);
    }

    public void EasyDifficulty()
    {
        SetDifficultyState(1, 0, 0);
        SetInitialDifficultyMode("easy");
    }

    public void MediumDifficulty()
    {
        SetDifficultyState(0, 1, 0);
        SetInitialDifficultyMode("medium");
    }

    public void HardDifficulty()
    {
        SetDifficultyState(0, 0, 1);
        SetInitialDifficultyMode("hard");
    }

	/*Scene Navigation*/
	public void BackToMainMenu()
    {
        SceneFader.Instance.ChangeSceneWithFader("MainMenu");
    }
    /*End Scene Navigation*/


}
