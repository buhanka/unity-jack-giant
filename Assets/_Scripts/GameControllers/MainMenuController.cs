﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour {

    [SerializeField]
    private Button musicButton;
    [SerializeField]
    private Sprite[] musicIcons;

	private void Start()
	{
        CheckMusicPlay();   
	}

    private void CheckMusicPlay()
    {
        if (GamePreferences.GetMusicState() == 1)
        {
            MusicController.Instance.PlayMusic(true);
            musicButton.image.sprite = musicIcons[0];
        }
        else
        {
            MusicController.Instance.PlayMusic(false);
            musicButton.image.sprite = musicIcons[1];
        }
    }

	/*Scene Navigation*/
	public void StartGame()
    {
        GameManager.Instance.gameStartedFromMainMenu = true;
        SceneFader.Instance.ChangeSceneWithFader("Gameplay");
    }

    public void HighScore()
    {
        SceneManager.LoadScene("HighScore");
    }

    public void Options()
    {
        SceneManager.LoadScene("Options");
    }
    /*End Scene Navigation*/


    public void QuitGame()
    {
        Application.Quit();
    }

    public void MusicButton()
    {
        if (GamePreferences.GetMusicState() == 0)
        {
            GamePreferences.SetMusicState(1);
            MusicController.Instance.PlayMusic(true);
            musicButton.image.sprite = musicIcons[0];
        }
        else if (GamePreferences.GetMusicState() == 1)
        {
            GamePreferences.SetMusicState(0);
            MusicController.Instance.PlayMusic(false);
            musicButton.image.sprite = musicIcons[1];
        }
    }
}
