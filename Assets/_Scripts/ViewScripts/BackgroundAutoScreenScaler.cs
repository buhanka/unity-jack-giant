﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundAutoScreenScaler : MonoBehaviour
{
    void Start()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        Vector3 tempScale = transform.localScale;

        float width = sr.bounds.size.x;

        float worldHeight = Camera.main.orthographicSize * 2f;
        float worldWidth = worldHeight / Screen.height * Screen.width;
        float localWidth = worldWidth / width;

        tempScale.x = localWidth;

        transform.localScale = tempScale;
    }
}
