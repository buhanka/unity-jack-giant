﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMotion : MonoBehaviour {

    //Public variables
    [HideInInspector]
    public bool moveCamera;
    //

    //Private variables
    private float speed = 1.0f;
    private float acceleration = 0.2f;
    private float maxSpeed = 3.2f;
    private float easySpeed = 2.8f;
    private float mediumSpeed = 3.4f;
    private float hardSpeed = 4.0f;
    //

    /*Unity methods*/
	private void Start () 
    {
        if (GamePreferences.GetEasyDifficultyState() == 1)
        {
            maxSpeed = easySpeed;
        }
        else if (GamePreferences.GetMediumDifficultyState() == 1)
        {
            maxSpeed = mediumSpeed;
        }
        else if (GamePreferences.GetHardDifficultyState() == 1)
        {
            maxSpeed = hardSpeed;
        }

        moveCamera = false;
	}
	
	private void Update () 
    {
        if (moveCamera)
        {
            MoveCamera();
        }
	}
    /**/

    private void MoveCamera()
    {
        Vector3 temp = transform.position;

        float oldY = temp.y;
        float newY = temp.y - (speed * Time.deltaTime);
        temp.y = Mathf.Clamp(temp.y, oldY, newY);

        transform.position = temp;

        speed += acceleration * Time.deltaTime;

        if (speed > maxSpeed)
        {
            speed = maxSpeed;
        }
    }
}
