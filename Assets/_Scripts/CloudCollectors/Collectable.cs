﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour {

    private void OnEnable()
    {
        Invoke("DestroyCollectable", 7f);
    }

    private void DestroyCollectable()
    {
        gameObject.SetActive(false);
    }
}
