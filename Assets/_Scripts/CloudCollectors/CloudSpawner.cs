﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudSpawner : MonoBehaviour {

    //Public variables

    //

    //Private variables
    [SerializeField]
    private GameObject[] clouds;
    private float distanceBetweenClouds;
    private float minX, maxX;
    private float lastCloudPositionY;
    private int controlX;
    [SerializeField]
    private GameObject[] collectables;
    [SerializeField]
    private GameObject player;
    //

    /*Unity methods*/
    private void Awake()
    {
        distanceBetweenClouds = 3.0f;
        controlX = 0;
        SetMinAndMaxX();
        CreateClouds();

        for (int i = 0; i < collectables.Length; i++)
        {
            collectables[i].SetActive(false);
        }
    }

    private void Start()
    {
        PositionThePlayer();
    }
    /**/

    private void SetMinAndMaxX()
    {
        Vector3 bounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));

        minX = -bounds.x + 0.5f;
        maxX = bounds.x - 0.5f;
    }

    private void Shuffle(GameObject[] arrayToShuffle)
    {
        for (int i = 0; i < arrayToShuffle.Length; i++)
        {
            GameObject temp = arrayToShuffle[i];
            int random = Random.Range(i, arrayToShuffle.Length);
            arrayToShuffle[i] = arrayToShuffle[random];
            arrayToShuffle[random] = temp;
        }
    }

    private void CreateClouds()
    {
        Shuffle(clouds);

        float positionY = 0;

        for (int i = 0; i < clouds.Length; i++)
        {
            Vector3 temp = clouds[i].transform.position;
            temp.y = positionY;

            //Use controlX for better spawn randomization
            switch (controlX)
            {
                case 0:
                    temp.x = Random.Range(0.0f, minX);
                    controlX = 1;
                    break;
                case 1:
                    temp.x = Random.Range(0.0f, maxX);
                    controlX = 2;
                    break;
                case 2:
                    temp.x = Random.Range(-1.0f, minX);
                    controlX = 3;
                    break;
                case 3:
                    temp.x = Random.Range(1.0f, maxX);
                    controlX = 0;
                    break;
                default:
                    break;
            }

            lastCloudPositionY = positionY;
            clouds[i].transform.position = temp;
            positionY -= distanceBetweenClouds; 
        }
    }

    private void PositionThePlayer()
    {
        GameObject[] deadlyClouds = GameObject.FindGameObjectsWithTag("Deadly");
        GameObject[] cloudsInGame = GameObject.FindGameObjectsWithTag("Cloud");

        for (int i = 0; i < deadlyClouds.Length; i++)
        {

            if (deadlyClouds[i].transform.position.y == 0)
            {
                Vector3 tempDeadlyPosition = deadlyClouds[i].transform.position;
                deadlyClouds[i].transform.position = cloudsInGame[0].transform.position;
                cloudsInGame[0].transform.position = tempDeadlyPosition;
            }
        }

        Vector3 temp = cloudsInGame[0].transform.position;

        for (int i = 1; i < cloudsInGame.Length; i++)
        {
            if (temp.y < cloudsInGame[i].transform.position.y)
            {
                temp = cloudsInGame[i].transform.position;
            }
        }

        temp.y += 1f;

        player.transform.position = temp;
    }

    /*Unity methods*/
    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.CompareTag("Cloud") || target.CompareTag("Deadly"))
        {
            if (target.transform.position.y == lastCloudPositionY)
            {
                Shuffle(clouds);
                Shuffle(collectables);

                Vector3 temp = target.transform.position;

                for (int i = 0; i < clouds.Length; i++)
                {
                    if (!clouds[i].activeInHierarchy)
                    {
                        //Use controlX for better spawn randomization
                        switch (controlX)
                        {
                            case 0:
                                temp.x = Random.Range(0.0f, minX);
                                controlX = 1;
                                break;
                            case 1:
                                temp.x = Random.Range(0.0f, maxX);
                                controlX = 2;
                                break;
                            case 2:
                                temp.x = Random.Range(-1.0f, minX);
                                controlX = 3;
                                break;
                            case 3:
                                temp.x = Random.Range(1.0f, maxX);
                                controlX = 0;
                                break;
                            default:
                                break;
                        }

                        temp.y -= distanceBetweenClouds;
                        lastCloudPositionY = temp.y;

                        clouds[i].transform.position = temp;
                        clouds[i].SetActive(true);

                        int random = Random.Range(0, collectables.Length);

                        if (!clouds[i].CompareTag("Deadly"))
                        {
                            if (!collectables[random].activeInHierarchy)
                            {
                                Vector3 temp2 = clouds[i].transform.position;
                                temp2.y += 0.7f;

                                if (collectables[random].name == "Life")
                                {
                                    if (PlayerScore.lifeCount < 2)
                                    {
                                        collectables[random].transform.position = temp2;
                                        collectables[random].SetActive(true);
                                    }
                                }
                                else
                                {
                                    collectables[random].transform.position = temp2;
                                    collectables[random].SetActive(true);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    /**/

}
