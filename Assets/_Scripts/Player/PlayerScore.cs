﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScore : MonoBehaviour {

    //Public variables
    public static int scoreCount;
    public static int lifeCount;
    public static int coinCount;
    //

    //Private variables
    [SerializeField]
    private AudioClip coinClip, lifeClip;
    private Vector3 previousPosition;
    private bool countScore;
    //

    /*Unity methods*/
    private void Start () 
    {
        previousPosition = transform.position;
        countScore = true;
	}
	
    private void Update () 
    {
        CountScore();
	}
    /**/

    private void CountScore()
    {
        if (countScore)
        {
            if (previousPosition.y > transform.position.y)
            {
                scoreCount++;
                GameController.Instance.SetScore(scoreCount);
            }

            previousPosition = transform.position;
        }
    }

    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.CompareTag("Coin"))
        {
            coinCount++;
            scoreCount += 200;

            AudioSource.PlayClipAtPoint(coinClip, transform.position);
            target.gameObject.SetActive(false);

            GameController.Instance.SetCoinScore(coinCount);
            GameController.Instance.SetScore(scoreCount);
        }

        if (target.CompareTag("Life"))
        {
            lifeCount++;
            scoreCount += 300;

            AudioSource.PlayClipAtPoint(lifeClip, transform.position);
            target.gameObject.SetActive(false);

            GameController.Instance.SetLifeScore(lifeCount);
            GameController.Instance.SetScore(scoreCount);
        }

        if (target.CompareTag("Bounds") || target.CompareTag("Deadly"))
        {
            OnDieActions();
        }
    }

    private void OnDieActions()
    {
        GameController.Instance.MoveCamera(false);
        countScore = false;
        transform.gameObject.SetActive(false);

        lifeCount--;
        GameManager.Instance.CheckGameStatus(scoreCount, coinCount, lifeCount);
    }
}
