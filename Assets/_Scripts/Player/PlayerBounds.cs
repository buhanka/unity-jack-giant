﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBounds : MonoBehaviour {

    //Private variables
    private float minX, maxX;
    //

    /*Unity methods*/
	private void Start () 
    {
        SetMinAndMaxX();
	}
	
	private void Update () 
    {
        if (transform.position.x < minX)
        {
            Vector3 temp = transform.position;
            temp.x = minX;
            transform.position = temp;
        }
        else if (transform.position.x > maxX)
        {
            Vector3 temp = transform.position;
            temp.x = maxX;
            transform.position = temp;
        }
	}
    /**/

    private void SetMinAndMaxX()
    {
        Vector3 bounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0.0f));

        minX = -bounds.x + 0.5f;
        maxX = bounds.x - 0.5f;
    }
}
