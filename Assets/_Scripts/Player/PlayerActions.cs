﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActions : MonoBehaviour {

    //Public variables
    public float speed = 8f;
    public float maxVelocity = 4f;
    //

    //Private variables
    private Rigidbody2D rb2d;
    private Animator anim;
    private float localXScale;
    //

    /*Unity methods*/
    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        localXScale = transform.localScale.x;
    }

    private void FixedUpdate()
    {
        if (GameController.Instance.cameraMotionState)
        {
            PlayerMovement();
        }
    }
    /**/

    //Handle player's movement
    private void PlayerMovement()
    {
        float forceX = 0f;
        float velocity = Mathf.Abs(rb2d.velocity.x);
        float horizontalAxis = Input.GetAxisRaw("Horizontal");

        if (horizontalAxis > 0)
        {
            //move right
            if (velocity < maxVelocity)
            {
                forceX = speed;

                ChangeToLeftSide(false);
                ChangeWalkAnimation(true);
            }
        }
        else if (horizontalAxis < 0)
        {
            //move left
            if (velocity < maxVelocity)
            {
                forceX = -speed;

                ChangeToLeftSide(true);
                ChangeWalkAnimation(true);
            }
        }
        else
        {
            //stop
            ChangeWalkAnimation(false);
        }

        rb2d.AddForce(new Vector2(forceX, 0));
    }
    //

    //Change animation state
    private void ChangeWalkAnimation(bool isWalk)
    {     
        if (isWalk)
        {
            anim.SetBool("Walk", true);
        }
        else
        {
            anim.SetBool("Walk", false);
        }
    }
    //

    //Change player scale based on walking side - left/right
    private void ChangeToLeftSide(bool isLeft)
    {
        if (isLeft)
        {
            Vector3 temp = transform.localScale;
            temp.x = -localXScale;
            transform.localScale = temp;
        }
        else
        {
            Vector3 temp = transform.localScale;
            temp.x = localXScale;
            transform.localScale = temp;
        }
    }
}
