﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveJoystick : MonoBehaviour {

    //Public variables
    public float speed = 8f;
    public float maxVelocity = 4f;
    //

    //Private variables
    private Rigidbody2D rb2d;
    private Animator anim;
    private float localXScale;
    private bool moveLeft, moveRight;
    //

    /*Unity methods*/
    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        localXScale = transform.localScale.x;
    }

	private void FixedUpdate()
	{
        if (GameController.Instance.cameraMotionState)
        {
            if (moveLeft)
            {
                MoveLeft();
            }

            if (moveRight)
            {
                MoveRight();
            }
        }
	}
    /**/

    public void SetSidesBasedOnLeft(bool left)
    {
        this.moveLeft = left;
        this.moveRight = !left;
    }

    public void StopMoving()
    {
        moveLeft = moveRight = false;
        ChangeWalkAnimation(false);
    }

    public void MoveLeft()
    {
        
        float forceX = 0f;
        float velocity = Mathf.Abs(rb2d.velocity.x);

        if (velocity < maxVelocity)
        {
            forceX = -speed;

            ChangeWalkAnimation(true);
            ChangeToLeftSide(true);

            rb2d.AddForce(new Vector2(forceX, 0));
        }
    }

    public void MoveRight()
    {
        float forceX = 0f;
        float velocity = Mathf.Abs(rb2d.velocity.x);

        if (velocity < maxVelocity)
        {
            forceX = speed;

            ChangeWalkAnimation(true);
            ChangeToLeftSide(false);

            rb2d.AddForce(new Vector2(forceX, 0));
        }
    }

    //Change animation state
    private void ChangeWalkAnimation(bool isWalk)
    {
        if (isWalk)
        {
            anim.SetBool("Walk", true);
        }
        else
        {
            anim.SetBool("Walk", false);
        }
    }
    //

    //Change player scale based on walking side - left/right
    private void ChangeToLeftSide(bool isLeft)
    {
        if (isLeft)
        {
            Vector3 temp = transform.localScale;
            temp.x = -localXScale;
            transform.localScale = temp;
        }
        else
        {
            Vector3 temp = transform.localScale;
            temp.x = localXScale;
            transform.localScale = temp;
        }
    }
}
